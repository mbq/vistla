#include "vistla.h"
#include <stdint.h>
#include <stdio.h>

//The junction data
#define N 50
#define M 7

const uint32_t D[M][N]={
  {1,2,1,2,2,2,3,3,2,3,2,3,3,2,2,3,2,3,2,2,3,4,2,4,2,1,2,3,3,2,3,2,2,2,3,3,1,3,2,2,3,2,3,2,1,3,2,3,2,2},
  {1,1,1,1,1,1,2,2,1,2,1,2,2,1,1,2,1,2,1,2,2,2,1,2,1,1,1,2,2,1,2,1,1,1,2,2,2,2,1,1,2,1,2,1,1,1,1,2,1,1},
  {1,1,1,1,1,1,2,2,1,2,1,2,2,1,1,2,1,2,1,2,2,2,1,2,1,1,1,2,2,1,2,1,1,1,2,2,2,2,1,1,2,1,2,1,1,2,1,2,1,1},
  {1,1,1,2,1,1,2,2,1,2,1,2,2,1,1,2,1,2,1,1,2,2,1,2,1,2,1,2,2,1,2,1,1,1,2,2,1,2,1,1,2,1,2,1,1,2,1,2,1,1},
  {1,1,1,1,1,1,2,2,1,2,1,2,2,1,1,2,1,2,1,2,2,1,1,2,1,2,1,2,2,1,2,1,1,1,2,2,2,2,1,1,2,1,2,1,1,1,1,2,1,1},
  {1,1,1,1,1,1,2,2,1,2,1,2,2,1,1,2,1,2,1,2,2,1,1,1,1,2,1,2,2,1,2,1,1,1,2,2,2,2,1,1,2,1,2,1,2,1,1,2,1,1},
  {1,2,1,2,2,2,2,1,2,1,2,1,1,2,2,1,2,1,2,2,1,2,2,2,2,1,2,1,1,2,1,2,2,2,1,1,1,1,2,2,1,2,1,1,1,1,2,1,2,2}
};
//Vistla expects an array of pointers to arrays with columns
const uint32_t* XX[M]={(uint32_t*)&D[0],(uint32_t*)&D[1],(uint32_t*)&D[2],
  (uint32_t*)&D[3],(uint32_t*)&D[4],(uint32_t*)&D[5],(uint32_t*)&D[6]};
const uint32_t Y[N]={2,2,2,2,2,2,1,1,2,1,2,1,1,2,2,1,2,1,2,1,1,1,2,1,2,1,2,1,1,2,1,2,2,2,1,1,1,1,2,2,1,2,1,2,2,2,2,1,2,2};
const uint32_t NX[N]={4,2,2,2,2,2,2};
const uint32_t NY=2;
const char* Xnames[]={"J","A1","A2","A3","B1","B2","B3"};
const char* Yname="Y";

void print_tree(struct vistla_branch *b,size_t depth){
  for(struct vistla_branch *x=b;x;x=x->next){
    if(!x->used) continue;
    if(depth==0) printf("%s->%s\n",Yname,Xnames[x->feature_a]);
    for(int e=0;e<depth+1;e++) printf(".");
    printf("%s->%s %s (%0.3f)\n",
      Xnames[x->feature_a],Xnames[x->feature_b],
      (x->leaf)?"leaf":"relay",x->score);
    if(x->deeper) print_tree(x->deeper,depth+1);
  } 
}

int main(int argc,char **argv){
  printf("Running vistla now; verbose logs:\n");
  struct vistla_output* ans=vistla_portable(
    N,M,(uint32_t**)XX,(uint32_t*)NX,(uint32_t*)Y,NY,
    0.,fromdown,21,1,true);
  printf("\nPrinting the resulting tree:\n");
  print_tree(ans->root,0);
  free_vistla_output(ans);
  return(0);
}
