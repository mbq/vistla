#ifndef __VISTLA_H__
#define __VISTLA_H__

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <math.h>


struct vistla_output {
 //Number of features
 uint32_t m;
 //Marix of feature-to-feature MI, i.e., I(X_i,X_j), column-wise
 double *mi;
 //Vector of I(X_i;Y)
 double *mi_y;
 //Matrix of previous pairs, column-wise. P[b,c]=a means (a,b)=>(b,c)
 uint32_t *previous_pair;
 //Score of pairs, column-wise. S[a,b] means the score to pair (a,b)
 double *score;
 //Vistla tree
 struct vistla_branch *root;
};

struct vistla_branch {
  uint32_t feature_a;
  uint32_t feature_b;
  double score;
  struct vistla_branch *next;
  struct vistla_branch *deeper;
  struct vistla_branch *super;
  bool leaf;
  bool used;
};

void free_vistla_output(struct vistla_output *x);

enum flow_mode {
 fromdown=10,
 intoup=5,
 spread=0,
 spread_noroam=16,
 both=3,
 both_noroam=19,
 down=8,
 up=4
};

struct vistla_output* vistla_portable(
  uint32_t n,uint32_t m,
  uint32_t **x,uint32_t *nx,
  uint32_t *y,uint32_t ny,
  double threshold,
  enum flow_mode flow_mode,
  uint32_t seed,size_t threads,bool verbose);








#endif /* __VISTLA_H__ */
