#include "vistla.h"
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <math.h>
//Sham OpenMP if not available
#ifdef _OPENMP
 #include <omp.h>
#else
 #define omp_get_thread_num() 0
 #define omp_get_max_threads() 1
 #define omp_set_num_threads(x)
#endif

typedef uint32_t u32;
typedef uint64_t u64;

//Sham R_alloc -- shouldn't b used anyway
static inline void* R_alloc(size_t c,size_t l){
 fprintf(stderr,"Try to R-alloc outside R --- aborting");
 abort();
 return(NULL);
}
#define NA_INTEGER 0
#define SEXP int
static inline void error(char *x){
 abort();
}
static inline void warning(char *x){
 abort();
}
static inline void GetRNGstate(){
 abort();
}
static inline void PutRNGstate(){
 abort();
}
static inline double unif_rand(){
 abort();
}
static inline bool asLogical(SEXP x){
 abort();
}
static inline double* REAL(SEXP x){
 return(NULL);
}
static inline int* INTEGER(SEXP x){
 return(NULL);
}
static inline size_t length(SEXP x){
 return(0);
}
#define TRUE true
#define R_NegInf (-INFINITY)
#define NA_REAL NAN
#define FALSE false
#define REALSXP 0
#define INTSXP 0
static inline SEXP PROTECT(SEXP x){
 abort();
}
static inline SEXP allocVector(int a,int b){
 abort();
}
static inline void UNPROTECT(int n){
 abort();
}
#define Rprintf printf

#include "../src/ht.h"
#include "../src/shared.h"
#include "../src/heap.h"
#include "../src/vistla.h"


static struct vistla_output* alloc_vistla_output(uint32_t m){
 struct vistla_output* ans=malloc(sizeof(struct vistla_output));
 ans->m=m;
 ans->mi=malloc(sizeof(double)*m*m);
 ans->mi_y=malloc(sizeof(double)*m);
 ans->previous_pair=malloc(sizeof(uint32_t)*m*m);
 ans->score=malloc(sizeof(double)*m*m);
 ans->root=NULL;
 return(ans);
}

void free_vistla_output(struct vistla_output *x){
 if(x->root) free(x->root);
 free(x->score);
 free(x->previous_pair);
 free(x->mi_y);
 free(x->mi);
 free(x);
}

struct vistla_output* vistla_portable(
  uint32_t n,uint32_t m,
  uint32_t **x,uint32_t *nx,uint32_t *y,uint32_t ny,
  double threshold,enum flow_mode flow_mode,uint32_t seed,size_t threads,bool verbose){
 
 enum flow flow=(enum flow)((uint32_t)flow_mode);
 if(threads<1){
  threads=omp_get_max_threads();
 }

 //Allocate hash tables, one per thread
 struct ht **ht=malloc(sizeof(struct ht*)*threads);
 for(size_t e=0;e<threads;e++) ht[e]=mallocHt(n);

 //Initiate RNG
 struct rng rng;
 set_rng(&rng,seed,1);

 //Allocate the priority queue
 struct heap *queue=mallocHeap(n);
 resetHeap(queue,n);

 //Alloc the outputs -- initiation is not needed, done in vistla call
 struct vistla_output *ans=alloc_vistla_output(m);
 double *mi=ans->mi;
 double *mi_y=ans->mi_y;
 uint32_t *P=ans->previous_pair;
 double *S=ans->score;
 
 uint32_t *si=malloc(sizeof(uint32_t*)*m*m);
  
 uint32_t branch_count=vistla(
   x,nx,y,ny,m,n,
   ht,&rng,threads,
   verbose,threshold,flow,
   NULL,m,mi,mi_y,queue,P,S,si);

 //Convert si into a proper tree
 bool *seen=malloc(sizeof(bool)*m);
 for(int e=0;e<m;e++) seen[e]=false;
 ans->root=malloc(sizeof(struct vistla_branch)*branch_count);
 for(size_t e=0;e<branch_count;e++){
  struct vistla_branch *x=&(ans->root[e]);
  x->feature_a=si[e]%m;
  x->feature_b=si[e]/m;
  x->score=S[si[e]];
  x->next=x->deeper=NULL;
  x->used=false;
  x->leaf=false;
  
  struct vistla_branch *sup=x->super=
    (P[si[e]]==NA_INTEGER)?NULL:&(ans->root[P[si[e]]-1]);
  
  if(!seen[x->feature_b]){
   seen[x->feature_b]=true;
   x->leaf=true;
   x->used=true;
   struct vistla_branch *mark=sup;
   for(;mark;mark=mark->super) mark->used=true;
   
  }
  if(sup){
   if(sup->deeper){
    struct vistla_branch *prev=sup->deeper;
    while(prev->next) prev=prev->next;
    prev->next=x;
   }else{
    sup->deeper=x;
   }
  }else if(x!=ans->root){
   struct vistla_branch *prev=ans->root;
   while(prev->next) prev=prev->next;
   prev->next=x;
  }
 }

 //Release memory
  
 //Priority queue
 freeHeap(queue);
 
 //Hash tables
 for(size_t e=0;e<threads;e++) freeHt(ht[e]);
 free(ht);

 free(si);
 free(seen);
 
 return(ans);
}
