# Vistla

[![CRAN status](https://www.r-pkg.org/badges/version/vistla)](https://CRAN.R-project.org/package=vistla)
[![CRAN downloads](https://cranlogs.r-pkg.org/badges/vistla)](https://cran.rstudio.com/web/packages/vistla/index.html) 
[![pipeline status](https://gitlab.com/mbq/vistla/badges/main/pipeline.svg)](https://gitlab.com/mbq/vistla/commits/main)

Vistla is an information-theory based detector of influence paths; this repository contains its reference implementation in a form of an R package.

The method is described in
- [Miron B. Kursa (2025). Vistla: identifying influence paths with information theory. Bioinformatics, btaf036.](https://doi.org/10.1093/bioinformatics/btaf036)

You can install from CRAN or directly from GitLab with:

```r
devtools::install_gitlab('mbq/vistla')
```
